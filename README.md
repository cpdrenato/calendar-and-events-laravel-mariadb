<p align="center"><img src="calander.png"></p>
 
# Calendar and Events - Laravel  + MariaDB

Calendar made with Laravel.

## Installation 
After download install composer

```bash
    composer install
    cp .env.example .env
    php artisan key:generate
    php artisan migrate
```
 

## Fonte
- https://www.tutofox.com/laravel/tutorial-laravel-calendario-crear-una-enevto-en-el-calendario/

## Developer By Renato
