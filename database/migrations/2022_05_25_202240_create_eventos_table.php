<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evento', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo', 11)->nullable();
            $table->string('descripcion', 55)->nullable();
            $table->date('fecha');
            $table->timestamps();
        });
        /*CREATE TABLE `evento` (
            `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
            `titulo` VARCHAR(11) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
            `descripcion` VARCHAR(55) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
            `fecha` DATE NOT NULL,
            `created_at` TIMESTAMP NULL DEFAULT NULL,
            `updated_at` TIMESTAMP NULL DEFAULT NULL,
            PRIMARY KEY (`id`) USING BTREE
        )
        COLLATE='utf8mb4_unicode_ci'
        ENGINE=InnoDB
        ;*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evento');
    }
}
